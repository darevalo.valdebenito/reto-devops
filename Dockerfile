#Image TAG
FROM node:alpine

#App directory
WORKDIR /usr/src/app

#App dependencies
COPY package*.json ./

RUN npm install

#Bundle app
COPY . .

#Exposing port
EXPOSE 3000

#Running as non-root
USER node

#Running command
CMD [ "node", "index.js" ]
